"""
test_examples.py
================
.. code-block:: XML

    Purpose:       Example python unit test script.
    Author:        Emmie King, Emmie@see.com
    Project:       FDT
    NDA/ITAR:      None
    Software:      Developed Python v.3.7
    Version:       1, release date TBD
    Input:         None
    Output:        None
    Copyright:     Space Exploration Engineering 2020
"""

import unittest


class test_examples(unittest.TestCase):

    def test_addition(self):

        """ Summary Line....

        Test addition unit test

        .. code-block:: XML

            Test 1:    assertEqual(int + int)
            Test 2:    assertEqual(float + float)
            Test 3:    assertEqual(string + string)

        """

        # Test 1:
        self.assertEqual(5 + 10, 15)

        # Test 2:
        self.assertEqual(5.00 + 10.00, 15)

        # Test 3:
        self.assertEqual("add" + " strings", "add strings")


if __name__ == '__main__':
    unittest.main()
